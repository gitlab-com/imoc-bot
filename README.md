# imoc-bot

A simple [slack slash-command](https://api.slack.com/slash-commands)-based bot for assisting with incident management.

Invoked through slack using the `/start-incident` slash command.

Currently, it is written as a Google Cloud Function implementation. This will be migrated to GitLab ChatOps once our `ops.gitlab.net` has been setup.

Until then, there is no point in running this on GitLab.com as it's purpose is to assist when GitLab.com is down.

# Deploying

1. `cp config.json.template config.json`
2. Edit `config.json`
3. `make`

