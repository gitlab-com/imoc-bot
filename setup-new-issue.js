"use strict";

const config = require("./config.json");
var httpClient = require("./http-client");

function setupNewIssue(context) {
  let projectId = encodeURIComponent(config.GITLAB_ISSUE_TRACKER);

  return httpClient(`https://gitlab.com/api/v4/projects/${projectId}/issues`, "POST", {
    title: context.title,
    description: `Incident reported.

Working Doc: ${context.workingDocUrl}

Details to follow...
`,
    labels: "incident"
  }).then(([body]) => {
    return body.web_url;
  });
}

module.exports = setupNewIssue;
