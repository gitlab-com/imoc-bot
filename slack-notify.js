"use strict";

const { IncomingWebhook } = require("@slack/client");
const config = require("./config.json");

function notifySlack(message) {
  const webhook = new IncomingWebhook(config.SLACK_WEBHOOK_URL);

  return webhook
    .send({
      channel: config.SLACK_CHANNEL,
      text: message
    })
    .then(() => {
      return {
        channel: config.SLACK_CHANNEL
      };
    });
}

module.exports = notifySlack;
