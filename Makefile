
all:
  # Make sure you've setup the config json before attempting a deploy
	test -f config.json
	gcloud --project gitlab-infra-automation beta functions deploy setupIncident --trigger-http
	gcloud --project gitlab-infra-automation beta functions deploy setupIncidentBackend --trigger-http
