"use strict";

const { google } = require("googleapis");
const config = require("./config.json");

function getDriveClient() {
  return google.auth
    .getClient({
      scopes: ["https://www.googleapis.com/auth/drive.file", "https://www.googleapis.com/auth/drive"]
    })
    .then(auth => {
      return google.drive({
        version: "v3",
        auth: auth
      });
    });
}
function newWorkingDoc(context) {
  return getDriveClient()
    .then(drive => {
      return drive.files.copy({
        fileId: config.ORIGINAL_WORKING_DOC_TEMPLATE,
        supportsTeamDrives: true,
        resource: {
          name: context.title,
          parents: [config.GOOGLE_DRIVE_FOLDER]
        }
      });
    })
    .then(resp => {
      return {
        url: `https://docs.google.com/document/d/${resp.data.id}/edit`
      };
    });
}

module.exports = newWorkingDoc;
