"use strict";

const setupNewIncident = require("./setup-new-incident");
const config = require("./config.json");
const createResponderForUrl = require("./create-responder-for-url");
const request = require("request");

exports.setupIncident = (req, res) => {
  let body = req.body;

  return Promise.resolve()
    .then(() => {
      if (!body || body.token !== config.SLACK_TOKEN) {
        let err = new Error("Token required");
        err.code = 401;
        throw err;
      }

      let requestOptions = {
        method: req.method,
        form: body,
        url: config.BACKEND_URL,
        json: true
      };

      // Async response to backend to continue the long-running part of the process
      request(requestOptions, function(err, response) {
        if (err) {
          console.log(`Backend call failed:  ${err.stack || err.message}`);
          return;
        }

        if (response.statusCode >= 400) {
          console.log(`Backend call failed:  ${response.statusCode}`);
          return;
        }

        console.log(`Backend call responded with:  ${response.statusCode}`);
      });
    })
    .then(() => {
      res.status(200).end();
    })
    .catch(err => {
      console.error(err.stack || err);

      res.status(err.code || 500).send(err.message);
    });
};

exports.setupIncidentBackend = (req, res) => {
  let body = req.body;

  return Promise.resolve()
    .then(() => {
      if (!body || body.token !== config.SLACK_TOKEN) {
        let err = new Error("Token required");
        err.code = 401;
        throw err;
      }

      let responder = createResponderForUrl(body.response_url);
      return setupNewIncident(body, responder);
    })
    .then(() => {
      res.status(200).end();
    })
    .catch(err => {
      console.error(err.stack || err);

      res.status(err.code || 500).send(err.message);
    });
};
