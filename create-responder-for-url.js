"use strict";

const { IncomingWebhook } = require("@slack/client");

function createResponderForUrl(responseUrl) {
  if (!responseUrl) {
    return message => {
      console.log("Response: ", message);
      return Promise.resolve();
    };
  }
  const webhook = new IncomingWebhook(responseUrl);

  return message => {
    console.log("Responder: ", message);
    return webhook.send({
      response_type: "in_channel",
      text: message
    });
  };
}

module.exports = createResponderForUrl;
