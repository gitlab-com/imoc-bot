"use strict";

const request = require("request");
const config = require("./config.json");

let gitlabToken = config.GITLAB_TOKEN;

function get(address, method, form) {
  console.error(`# ${method || "GET"} ${address}`);

  return new Promise(function(resolve, reject) {
    let requestOptions = {
      method: method || "GET",
      form: form,
      url: address,
      json: true,
      gzip: true,
      headers: {
        "Private-Token": gitlabToken
      }
    };

    request(requestOptions, function(error, response, body) {
      if (error) return reject(error);
      if (response.statusCode >= 400) {
        return reject(new Error(`HTTP ${response.statusCode}`));
      }

      resolve([body, response]);
    });
  });
}

module.exports = get;
