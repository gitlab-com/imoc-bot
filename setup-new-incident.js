"use strict";

const slackNotify = require("./slack-notify");
const newWorkingDoc = require("./setup-working-doc");
const setupNewIssue = require("./setup-new-issue");
const config = require("./config.json");
const format = require("date-fns/format");

function setupNewIncident(body, responder) {
  let userId = body.user_id;
  let formattedDate = format(new Date(), "YYYY-MM-DD");

  let context = {
    title: `Incident Working doc: ${formattedDate}`
  };

  return responder(`<@${userId}> has initiated a new incident response. Please standby while I set it up...`)
    .then(() => {
      return newWorkingDoc(context, body);
    })
    .then(resp => {
      context.workingDocUrl = resp.url;
      return responder(`Incident doc has been created at ${context.workingDocUrl}`);
    })
    .then(() => {
      return setupNewIssue(context, body)
        .then(url => {
          context.issueUrl = url;
          return responder(`Created an issue to track this on GitLab.com: <${context.issueUrl}>`);
        })
        .catch(err => {
          return responder(`Unable to create an issue on GitLab.com for this issue. This will have to be done manually: \`${err.message}\``);
        });
    })
    .then(() => {
      let messages = [`:pager: <@${userId}> has initiated a new incident.`];
      messages.push(`:page_facing_up: Working doc is at <${context.workingDocUrl}>.`);

      if (context.issueUrl) {
        messages.push(`:gitlab: Issue created at <${context.issueUrl}>`);
      }

      messages.push(`:zoom: call is ${config.ZOOM_URL}`);

      return slackNotify(messages.join("\n"));
    })
    .then(resp => {
      return responder(`:slack: Slack notification posted to ${resp.channel}`);
    })
    .catch(err => {
      console.error(err.stack || err);
      return responder(`:disappointed: Sorry to report, but incident setup failed: \`${err.message}\``);
    });
}

module.exports = setupNewIncident;
